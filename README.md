# esp-rfid-desfire

Building an RFID terminal with ESP32 which can read/write Mifare Desfire EV1+EV2 tokens

Inspired by this excellent work by Elmü: https://www.codeproject.com/Articles/1096861/DIY-electronic-RFID-Door-Lock-with-Battery-Backup


## Testing the RFID hardware on Ubuntu Linux 18.04
Before getting started with the Arduino/ESP, I decided to test my RFID reader hardware with libnfc and an FTDI USB-to-serial adapter first. This was a bit tricky so here are the steps I needed to take (thanks to https://firefart.at/post/how-to-crack-mifare-classic-cards/):
```
git clone https://github.com/nfc-tools/libnfc.git
cd libnfc/
sudo apt-get install libusb-0.1 libtool libusb-dev libpcsclite-dev pkg-config
aclocal
autoreconf -vis
./configure --with-drivers=pn532_uart --enable-serial-autoprobe --sysconfdir=/etc --prefix=/usr
make
sudo make install
```

And soon enough I was able to read several different tokens (mostly I'm interested in keyfobs), including Mifare Classic 1k, Desfire EV1 1k, and Desfire EV2 4k. The Adafruit reader even read 4 tags at once when I put a bag of 15 Mifare Classic 1k Keyfobs on it.

I successfully tested 2 different Adafruit-PN532-clones (12cm long cards with rather large antennas), one bought in Germany and one on Aliexpress, and a Elechouse PN532 reader (43x41mm) which, despite Elmü's perfectly valid warnings, I really want to use in the long run because I want to integrate it into a lightswitch (blind cover) at my front foor. For that I don't need more than 1-2cm reading distance. It reads the EV1+EV2 keyfob UID's at 2-3cm distance but I still have to see if the Desfire encryption works. I can live with 1cm rading distance through the thin plastic cover.

## First test with ESP32
With the Adafruit reader configured to SPI and the library set to "software SPI", I assigned all the control pins in main.cpp (<-- converted from DoorOpener.ino because I prefer using platformio) to the ones that would also match the hardware SPI of the ESP32 (SPI channel #3 called VSPI), gave it a quick run and... right away I could see UID's of tags being displayed:

```Unknown person tries to open the door: 8C 62 D3 89 00 00 00
> Unknown person tries to open the door: 04 E3 6F BA 8D 60 80
> Unknown person tries to open the door: F5 EC 2A 1B 00 00 00
> Unknown person tries to open the door: B5 B7 CA 1A 00 00 00
> Unknown person tries to open the door: 35 C4 D8 1A 00 00 00
```

Unfortunately the EEPROM functions for the ESP32 work differently than for Teensy, so many changes to the usermanager are required. It might be easier to use the Preferences library. I need to see which one is easier to adapt to.

Looking at https://randomnerdtutorials.com/esp32-save-data-permanently-preferences/ it seems Preferences.h is nice and easy to use, but I don't like the fact that the "key" is always a string. This is a waste of space because I'd rather use uint64 as a key to store the card's ID, which would require less space (8 Bytes instead of at least 15 for an 8-byte hex string plus null termination) and not require conversion.

Meanwhile I've decided to use LittleFS (lfs), which is a successor of SPIFFS. After using the usermanager.h from https://github.com/mruettgers/Doorguard (with a few small mods) which uses SPIFFS and EDB I got the selftest to run successfully with a Desfire EV2 keyfob. One thing I noticed was that it would not work reliably with the keyfob lying directly on the reader coil. After adding a plastic spacer of ~15mm it worked well.

On a side note, I also tried the sqlite3 library, after reading the docs I realized it was way overkill for this project, but since I love using SQL I decided to try it. But it added 350kB to the project - more than doubling it ;-)

More info sources on RFID readers:
* https://forum.mikroe.com/viewtopic.php?f=147&t=64203 - improve performance of cheap MFRC522 reader by replacing inductances (L1,L2 from antenna replaced by 2200nH/200mA)
* https://www.nxp.com/docs/en/nxp/application-notes/AN11056.pdf PN533 application note

